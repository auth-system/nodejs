# NodeJS

Back-end of an authentication system, created using NodeJS, ExpressJS, and MongoDB.

## Endpoints

POST Create Account - Username, Password
POST Login - Username, Password
POST Logout - Session Token
