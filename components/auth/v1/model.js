const { model, Schema } = require('mongoose')

const validator = require('../../../utils/validator')

const fields = {
	password: value => {
		const patt = /^[a-zA-Z0-9!@#$%^&*]+$/
		return patt.test(value)
	},
	username: value => {
		const patt = /^[a-zA-Z0-9 ]+$/
		return patt.test(value)
	},
}

const schemaToken = new Schema({ expiry: Date, token: String })

const schema = {
	active_tokens: [schemaToken],
	password: { required: true, type: String },
	username: { required: true, type: String },
}

const schemaObj = Schema(schema)

module.exports.model = model('user', schemaObj)
module.exports.schema = schema
module.exports.validator = (req, res, next) =>
	validator(fields, req, res, schema, true, next)
module.exports.validatorNS = (req, res, next) =>
	validator(fields, req, res, schema, false, next)
