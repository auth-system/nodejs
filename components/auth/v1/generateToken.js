// Function to generate JSON Web Token for user authentication

const { sign } = require('jsonwebtoken')

module.exports = (id, expiresIn = null) => {
	if (expiresIn) return sign({ id }, process.env.JWT_SECRET, { expiresIn })
	else return sign({ id }, process.env.JWT_SECRET)
}
