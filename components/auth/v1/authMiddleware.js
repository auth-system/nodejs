const { verify } = require('jsonwebtoken')

const userModel = require('./model').model

module.exports = async (req, res, next) => {
	const token = req.cookies.auth_token
	const tokenRefresh = req.cookies.refresh_token

	if (!tokenRefresh) return res.status(401).json({ msg: 'Token required' })

	try {
		const decoded = verify(token, process.env.JWT_SECRET)
		const user = await userModel.findById(decoded.id)
		req.user = user

		next()
	} catch (err) {
		try {
			const decoded = verify(tokenRefresh, process.env.JWT_SECRET)
			const user = await userModel.findById(decoded.id)

			const currDate = Date.now()
			let isTokenActive = false

			user.active_tokens = user.active_tokens.filter(active_token => {
				if (active_token.expiry < currDate) return false
				if (active_token.token === tokenRefresh) isTokenActive = true
				return true
			})

			await user.save()

			if (!isTokenActive)
				res.status(401).json({ msg: 'Refresh Token is invalid' })

			const newToken = require('./generateToken')(user._id, '1h')
			res.cookie('auth_token', newToken, { httpOnly: true })

			req.user = user
			next()
		} catch (err2) {
			console.error(err)
			console.error(err2)
			return res.status(401).json({ msg: 'Refresh Token is invalid' })
		}
	}
}
