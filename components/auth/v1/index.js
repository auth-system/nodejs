const express = require('express')
const { compare, genSaltSync, hashSync } = require('bcryptjs')
const { decode } = require('jsonwebtoken')

const authMiddleware = require('./authMiddleware')
const model = require('./model')
const internalError = require('../../../utils/internalError')

const userModel = model.model
const router = express.Router()
const validator = model.validator

router.post('/create', validator, async (req, res) => {
	try {
		const { username, password } = req.body

		const salt = genSaltSync(10)
		const hash = hashSync(password, salt)

		let user = await userModel.findOne({ username })
		if (user) return res.status(400).json({ msg: 'Username exists' })

		let newUser = new userModel({ username, password: hash })

		newUser = await newUser.save()

		res.json({
			msg: `Creating account with username ${username} and password ${password}`,
		})
	} catch (err) {
		internalError(err, res)
	}
})

router.post('/login', validator, async (req, res) => {
	try {
		const { username, password } = req.body

		let user = await userModel.findOne({ username })
		if (!user) return res.status(401).json({ msg: 'Username is invalid' })

		const isMatch = await compare(password, user.password)
		if (!isMatch) return res.status(401).json({ msg: 'Password is invalid' })

		const token = require('./generateToken')(user._id, '1h')
		const tokenRefresh = require('./generateToken')(user._id)

		let tokenRefreshExpiry = new Date()
		tokenRefreshExpiry = tokenRefreshExpiry.getTime() + 60 * 60 * 1000

		user.active_tokens.push({ token: tokenRefresh, expiry: tokenRefreshExpiry })
		await user.save()

		user.password = undefined
		res.cookie('auth_token', token, { httpOnly: true })
		res.cookie('refresh_token', tokenRefresh, { httpOnly: true })
		res.json({ msg: 'Login Successful' })
	} catch (err) {
		internalError(err, res)
	}
})

router.use(authMiddleware)

router.get('/logout', (req, res) => {
	res.json({ msg: `Logging out` })
})

module.exports = router
