const express = require('express')
const router = express.Router()

const authV1 = require('../components/auth/v1')

router.use('/auth', authV1)

module.exports = router
