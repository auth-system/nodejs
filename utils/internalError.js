// Function to respond with server error

module.exports = (err, res, msg = 'Server Error') => {
	console.error(err)
	res.status(500).send({ msg })
}
