const DB_URL = 'mongodb://localhost/auth-system'
const mongoose = require('mongoose')

const db = mongoose.connection

mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })

db.on('error', console.error.bind(console, 'Connection error:'))

db.once('open', () =>
	console.log('Connection success: Auth System Database connected.')
)
