// Function to perform validation on database records

module.exports = (fields, req, res, schema, strict, next) => {
	// Check if unwanted fields are not included in the body
	const bodyKeys = Object.keys(req.body).sort()
	const schemaKeys = Object.keys(schema).sort()

	bodyKeys.map(key => {
		if (!schemaKeys.includes(key))
			return res.status(400).json({ msg: 'Invalid request' })

		return 1
	})

	// Array to store all validation errors
	const errors = []

	for (let field in schema) {
		const value = req.body[field]
		const { required } = schema[field]

		// Check if required field is present
		if (strict && required && !value) {
			errors.push(field + ' is required')
			continue
		}

		// Check if value is valid
		if (value && !fields[field](value)) errors.push('Invalid ' + field)
	}

	// Respond with errors if they exist
	if (errors.length > 0) return res.status(400).json({ msg: errors })

	next()
}
