require('dotenv').config()

if (!process.env.JWT_SECRET) {
	console.log(
		"Please set the following environtment variables to start:-\n\nJWT_SECRET: secret key used to generate JWT tokens.\n\nYou can either set it as a global variable in your machine or\nsave it in a '.env' file in the root of the project directory.\nVisit https://www.npmjs.com/package/dotenv for more info."
	)
} else {
	require('./express')
	require('./utils/mongoose')
}
