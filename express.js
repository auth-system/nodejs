const cookieParser = require('cookie-parser')
const express = require('express')

const routeV1 = require('./routes/v1')

const app = express()
const PORT = 3000

app.use(cookieParser())
app.use(express.json())

app.use('/v1', routeV1)

app.listen(PORT, () =>
	console.log(`Example app listening at http://localhost:${PORT}`)
)
